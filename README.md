![phan-tich-binh-luan-icon](/uploads/17e4aa3083821925f5a53e49c203fa7f/phan-tich-binh-luan-icon.png)



**Overview:**

This is a public repository of Facebook Bulky Ads Manager application. 

This application is made to help professional Facebook advertisers can manage their massive number of ads easily. It saves you a ton of time. With Bulky Ads Manager, we do not have to open tens of browser tab and excel files. It helps to save 50% effort in ads management.

**Authority:**

Author's name: Bui Thanh Son

Author's email: no.way.to.come.back@gmail.com


**Core feature:**
- View, start, pause facebook ads campaigns, adsets, adses from mutiple ad accounts.
- Detail statistical report for one or multiple campaigns, adsets, adses.
- Filtering and searching campaigns, adsets, adses quickly.
- List all ads that running efficiently or inefficiently according to defined rules.
 

**Privacy Policy:**

We have Copyright Ownership of this application.

This application's purpose is to support us own business only, if you think it also can help your business and would like to try, please contact us first. All of the uncopyrighted usages of the application are illegal.

To use the application you must provide your ads_management, ads_read permission from your Facebook account. You can see more at https://developers.facebook.com/docs/facebook-login/permissions/v3.0#reference-ads_management and https://developers.facebook.com/docs/facebook-login/permissions/v3.0#reference-ads_read

This application is a tool purely, it gets your Facebook ads information then display in a convenient and easy to read ways. We are not responsible for anything related to your ads.

We do not collect any data of application user. All your ads data(content, targeting, insight) are secured and not stored in anywhere even your machine local storage.


**Application screenshots:**

![Screen_Shot_2018-06-01_at_5.48.04_PM](/uploads/67e815cf0c455f2c16719c18e00ae419/Screen_Shot_2018-06-01_at_5.48.04_PM.png)

![Screen_Shot_2018-06-01_at_5.48.50_PM_1](/uploads/bc5d6d9e2ff7b88e2ecdc97d9a2d135e/Screen_Shot_2018-06-01_at_5.48.50_PM_1.png)

![Screen_Shot_2018-06-01_at_5.49.06_PM](/uploads/6b5108a38c3252b2b32d68ce2013547e/Screen_Shot_2018-06-01_at_5.49.06_PM.png)

![Screen_Shot_2018-06-01_at_5.49.25_PM](/uploads/59fea2e3797d0f898178bfd5c21cc567/Screen_Shot_2018-06-01_at_5.49.25_PM.png)

![Screen_Shot_2018-06-01_at_5.50.57_PM](/uploads/6f4edcca36b0f59ab615c2f73953853f/Screen_Shot_2018-06-01_at_5.50.57_PM.png)

